﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class scrVolverMenu : MonoBehaviour {

	public Text puntuacion;
    void Start()
	{
		if(puntuacion != null)
		    puntuacion.text = PlayerPrefs.GetInt("Puntuacion").ToString();

	}
	
	// Update is called once per frame
	void Update ()
	{
		if(Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Space))
		{
			SceneManager.LoadScene(0);
		}
	}
}
