﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class scrMenu : MonoBehaviour {
	int pointer;
	Text[] opciones;

	// Use this for initialization
	void Awake () {
		opciones = new Text[transform.childCount];
		for (int i = 0; i < transform.childCount; i++)
		{
			opciones[i] = transform.GetChild(i).gameObject.GetComponent<Text>();
			opciones[i].color = Color.green;
	    }
		pointer = 0;
		opciones[pointer].color = Color.black;
		GetButton().GetComponent<Animator>().SetBool("Girando", true);
       
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.UpArrow))
		{
			GetButton().GetComponent<Animator>().SetBool("Girando", false);
			opciones[pointer].color = Color.green;
			pointer--;
			if (pointer < 0)
			{
				pointer = opciones.Length - 1;

			}
			opciones[pointer].color = Color.black;
			GetButton().GetComponent<Animator>().SetBool("Girando", true);
		}
		else if (Input.GetKeyDown(KeyCode.DownArrow))
		{
			GetButton().GetComponent<Animator>().SetBool("Girando", false);
			opciones[pointer].color = Color.green;
			pointer++;
			if (pointer > opciones.Length - 1)
			{
				pointer = 0;

			}
			opciones[pointer].color = Color.black;
			GetButton().GetComponent<Animator>().SetBool("Girando", true);
		}
		else if (Input.GetKeyDown(KeyCode.Return)||Input.GetKeyDown(KeyCode.Space) )
		{
			switch(pointer)
			{
				case 0:
					SceneManager.LoadScene(1);
					break;
				case 1:
					SceneManager.LoadScene(2);
					break;
				case 2:
					SceneManager.LoadScene(5);
					break;
				case 3:
					Application.Quit();
					break;
					
			}

		}
		
	}

	public GameObject GetButton()
	{
		return opciones[pointer].transform.GetChild(0).gameObject;
    }
}
