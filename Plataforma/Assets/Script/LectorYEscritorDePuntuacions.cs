﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LectorDePuntuaciones
{
	private int[] puntuacionesFinales;
	private  static int MAX_PUNTUATION = 6;

	public int[] Leer()
	{
		puntuacionesFinales = new int[MAX_PUNTUATION];
		for (int i = 0; i < MAX_PUNTUATION; i++)
		{
			puntuacionesFinales[i] = PlayerPrefs.GetInt((i + 1).ToString(),0);

		}
		return puntuacionesFinales;

	}

	public void Escribir(int nuevaPuntuacion)
	{
		int aux = 0;
		int[] todasPuntuaciones = Leer();
		for (int i = 0; i < MAX_PUNTUATION; i++)
		{
			if(todasPuntuaciones[i] < nuevaPuntuacion)
			{
				aux = todasPuntuaciones[i];
				PlayerPrefs.SetInt((i + 1).ToString(),nuevaPuntuacion);
				MoverUnoHaciaAbajo(aux, i+1, todasPuntuaciones);
				break;
			}
		}
	}

	public void MoverUnoHaciaAbajo(int aux, int index, int[] puntuaciones)
	{
		for (int i = index; i < MAX_PUNTUATION; i++)
		{

			PlayerPrefs.SetInt((i + 1).ToString(), aux);
			aux = puntuaciones[i];
		}
		
	}



}