﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scrPlataformaRelantizadora : MonoBehaviour {


    private bool YA_LA_HE_TOCADO_ASI_QUE_NO_RECIBES_MAS_PUNTOS = false;
    private static int JUMPFORCE = 5;
    // Use this for initialization
    void Start()
    {
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            if (!YA_LA_HE_TOCADO_ASI_QUE_NO_RECIBES_MAS_PUNTOS)
            {
                col.gameObject.SendMessage("MasPuntuacion");
                YA_LA_HE_TOCADO_ASI_QUE_NO_RECIBES_MAS_PUNTOS = !YA_LA_HE_TOCADO_ASI_QUE_NO_RECIBES_MAS_PUNTOS;
            }

            GameObject playerObject = col.gameObject;
			scrPlayer playerScript = playerObject.GetComponent<scrPlayer>();
			playerScript.Relantizarse();           
        }

    }
}
