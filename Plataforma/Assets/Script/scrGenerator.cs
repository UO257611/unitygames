﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scrGenerator : MonoBehaviour {
	public GameObject[] listOfPossibles;
	public float minSpwan=1F;
	public float maxSpawn = 2F;
	// Use this for initialization
	void Start () {
		Spawn ();
	}
	
	// Update is called once per frame
	void Spawn () {
		Instantiate (listOfPossibles [Random.Range (0,listOfPossibles.GetLength (0))], transform.position, Quaternion.identity);
		Invoke ("Spawn", Random.Range (minSpwan, maxSpawn));
	}
}
