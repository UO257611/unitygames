﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scrCamera : MonoBehaviour {
	public GameObject player;
	private float x;
	// Use this for initialization
	void Start () {
		x = player.transform.position.x;
	}
	
	// Update is called once per frame
	void Update () {
		float tempX = player.transform.position.x;

		if (tempX != x) {
			float difference = tempX - x;
			this.transform.position = new Vector3 (this.transform.position.x + difference,  this.transform.position.y,this.transform.position.z);

		}

		x = tempX;

		
	}
}
