﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class scrDestructor : MonoBehaviour {

	public Text puntuacionActual;
	public void OnTriggerEnter2D(Collider2D coll) {
		if(coll.tag=="Player"){

			scrPlayer player = coll.gameObject.GetComponent<scrPlayer>();

            PlayerPrefs.SetInt("Puntuacion", player.GetPuntuacion());   

			if (player.IsHighScore())
			{
				player.GuardarPuntuacion();      

                SceneManager.LoadScene(3);
			}
			else
			{
				SceneManager.LoadScene(4);
			}
            
		}
		else
		{
			Destroy(coll.gameObject);
		}
	}

}
