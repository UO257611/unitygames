﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scrBackground : MonoBehaviour {
	public float backgroundSize;

	private Transform myCamera;
	private ArrayList layers;
	public GameObject limite;
	public GameObject foto;
	public GameObject aparacion;

    
	// Use this for initialization
	void Start () {
		layers = new ArrayList();
		myCamera = Camera.main.transform;

		myCamera = Camera.main.transform;
        
		for (int i = 0; i < transform.childCount; i++)
		{
			layers.Add(transform.GetChild(i));
		}


	}
    
    private void ScrollRight()
	{

		GameObject x = Instantiate(foto, aparacion.transform.position, Quaternion.identity);
		layers.Add(x.transform);

        
	}





	// Update is called once per frame
	void FixedUpdate () {
		
		if ( CheckAllBackground() >= 0)
		{
			ScrollRight();
                    
		}
		
	}

    int CheckAllBackground()
	{
		for (int i = 0; i < layers.Count; i++)
		{
			
				Transform x = (Transform)layers[i];
				if (x.position.x <= limite.transform.position.x)
				{
					layers.RemoveAt(i);
					Destroy(x.gameObject);
					return 1;
				}

		}
		return -1;

	}
}
