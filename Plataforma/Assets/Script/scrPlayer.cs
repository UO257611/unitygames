﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scrPlayer : MonoBehaviour {
	private static float INITIAL_VELOCITY = 11.5F;
	public Rigidbody2D rb;
	public Text txtPuntuacion;
	public int puntuacionActual;
	public bool isGrounded;
	public Transform groundCheck;
	public float checkRadius;
	public LayerMask whatIsGround;
	private Animator animator;
	public int numAnimation = 0;
	private LectorDePuntuaciones lector;

	public float GRAVITY= 200;
	public float JUMPFORCE= 5 ;
	public float VELOCITY = INITIAL_VELOCITY;
	public int DOUBLEJUMP = 1;

	// Use this for initialization
	void Start () {
		lector = new LectorDePuntuaciones();
		puntuacionActual = 0;
		rb = GetComponent<Rigidbody2D> ();
		rb.freezeRotation = true;
		animator = GetComponent<Animator>();

	}

	void Update()
	{
		isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatIsGround);
        rb.velocity = new Vector2(VELOCITY, rb.velocity.y);

        if (isGrounded)
            DOUBLEJUMP = 1;
        if ((Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0)) && isGrounded)
        {
			rb.velocity = new Vector2(rb.velocity.x, JUMPFORCE);
            UpdateAnimation();        
            DOUBLEJUMP = 1;
        }
        ComprobarAnimacionLimites();
        if ((Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0)) && !isGrounded && DOUBLEJUMP > 0)
        {
            rb.velocity = new Vector2(rb.velocity.x, JUMPFORCE);
            DOUBLEJUMP--;
			UpdateAnimation();
        }

		if (VELOCITY < 30)
		    VELOCITY +=0.005F;

	}


	void MasPuntuacion()
	{
		puntuacionActual += 10;
		txtPuntuacion.text = puntuacionActual.ToString();

	}
	public bool IsHighScore()
	{
		int[] guardados = lector.Leer();

		for (int i = 0; i < guardados.Length; i++)
		{
			if (guardados[i] < puntuacionActual)
			{
				return true;
			}
		}
		return false;



	}

    public void UpdateAnimation()
	{
		numAnimation = PlayerPrefs.GetInt("animationPlayer", 0);
		numAnimation++;
		animator.SetInteger("TipoDeAnimacion", numAnimation);
        ComprobarAnimacionLimites();
		PlayerPrefs.SetInt("animationPlayer", numAnimation);
	}
   



    public void GuardarPuntuacion()
	{
		if( IsHighScore())
		{
			lector.Escribir(puntuacionActual);
		}

	}


    void ComprobarAnimacionLimites()
	{
		if (numAnimation >= 2)
			numAnimation = 0;
	}

    public int GetPuntuacion()
	{
		return puntuacionActual;
	}

    public void Relantizarse()
	{
		VELOCITY = INITIAL_VELOCITY;
	}
}
